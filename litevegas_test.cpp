#include "src/ipow.h"
#include "src/litevegas.h"

#include <fstream>
#include <iostream>
#include <random>

using std::cout;
using std::endl;
using std::string;

template <size_t n>
std::vector<double> power(const std::vector<double>& x, const double& w)
{
    std::vector<double> foo(n+1,0.);
    for (size_t i = 0; i < n+1; ++i)
        for (auto xi : x) foo[i] += ipow(xi,i);
    return foo;
}

template <size_t n>
double mylog(const std::vector<double>& x, const double& w)
{
    double foo = 1.;
    for (auto xi : x) foo *= ipow(log(xi),n);
    return foo;
}

std::vector<double> badsing(const std::vector<double>& x, const double& w)
{
    return std::vector<double>({x[0]-x[1]});
}

int main()
{
    cout << "LiteVegas" << endl;
    
    std::mt19937 gen;
    std::uniform_real_distribution<> distr(0.,1.);
    cout << "One random number: " << distr(gen) << endl;
    std::function<double(void)> ran = [gen,distr]() mutable->double {return distr(gen);};
    
    Vegas myvegas(ran,3,50,true);
    
    myvegas.setCalls(10000);
    myvegas.setInfoStream(&std::cout).setIterStream(&std::cout);
    
    cout << myvegas.integrate(power<2>) << endl;
    
    myvegas.setDimension(2).setCalls(10000);
    cout << myvegas.integrate(mylog<3>) << endl;
    myvegas.setGridStream(&std::cout).printGrid();
    std::ofstream grid2;
    grid2.open("/Users/lionetti/Desktop/grid2.dat");
    myvegas.setGridStream(&grid2).printGrid();
    grid2.close();
    
    myvegas.setMaxBins(20).setCalls(10000);
    cout << myvegas.integrate(badsing) << endl;
    myvegas.setGridStream(&std::cout).printGrid();
    
    std::ifstream inGrid;
    inGrid.open("/Users/lionetti/Desktop/grid2.dat");
    myvegas.readGrid(inGrid);
    myvegas.setGridStream(&std::cout).printGrid();
    
    return 0;
}
