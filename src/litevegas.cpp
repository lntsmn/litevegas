/**
 *
 * @file    litevegas.cpp
 * @author  Simone Lionetti
 * @date    May 2016
 *
 */

#include "litevegas.h"
#include "ipow.h"

// Scientific notation macro
#define SCI(x,w,p) std::scientific << std::setw((w)) << std::setprecision((p)) << (x)

// Constructors and destructor
//----------------------------------------------------------------------------------------

// Constructor with data
Vegas::Vegas(
             Random& random,
             const size_t& dim,
             const size_t& maxBins,
             const bool tryStratification
             ) :
_dim(dim),
_alpha(1.5),
_random(random),
_tryStrat(tryStratification), _useStrat(false),
_frozen(false), _smoothen(true),
_maxBins(maxBins), _bins(_maxBins), _totBins(ipow(_bins,_dim)), _boxes(1),
_grid(boost::extents[_maxBins+1][_dim]), _distr(boost::extents[_maxBins][_dim]),
_callsXiter(0), _callsXbox(0),
_maxIter(100), _absError(0.), _relError(0.),
_iter(0), _evals(0), _best(), _sumVals(0.), _sumWgts(0.), _sumSqrs(0.),
_oinfo(), _oiter(), _ogrid(), _odist(),
_x(_dim), _bin(_dim)
{
    resetGrid();
    clearDistr();
    return;
}

// Printing
//----------------------------------------------------------------------------------------

// Print setup parameters and the head for iteration-by-iteration results
const Vegas& Vegas::printInfo() const
{
    if (_oinfo && _oinfo->good()) {
        *_oinfo << "Number of dimensions = " << _dim << "\n";
        *_oinfo << "Completed " << _iter << " iterations of (max) " << _maxIter;
        *_oinfo << ", " << _callsXiter << " calls each.\n";
        *_oinfo << "Alpha = " << _alpha << "\n";
        *_oinfo << "Using " << _bins << " bins of " << _maxBins << " maximum\n";
        if (_useStrat) *_oinfo << "Using stratification, boxes = " << _boxes << "\n";
        else *_oinfo << "Using importance, boxes = " << _boxes << "\n\n";
    }
    // If printing iterations to terminal, make them more readable adding a header
    if (_oiter == &std::cout) {
        *_oiter << "              single.......iteration                  ";
        *_oiter << "accumulated.......results\n";
        *_oiter << "iteration        integral      sigma             integral";
        *_oiter << "      sigma     chi-sq/it\n";
        _oiter->flush();
    }
    return *this;
}

// Print line with current iteration's result
const Vegas& Vegas::printIter(const Point& curresult, const Point& cursigma) const
{
    if (_oiter && _oiter->good()) {
        *_oiter << std::setw(9) << _iter;
        for (size_t i = 0; i < curresult.size(); ++i)
            *_oiter << SCI(curresult[i],16,4) << SCI(cursigma[i],11,2);
        for (const auto& b : _best)
            *_oiter << SCI(b.value,21,4) << SCI(b.error,11,2) << SCI(b.chisq,14,2);
        *_oiter << std::endl;
    }
    return *this;
}

// Print line with current iteration's result
const Vegas& Vegas::printIter(const double& curresult, const double& cursigma) const
{
    if (_oiter && _oiter->good()) {
        *_oiter << std::setw(9) << _iter;
        *_oiter << SCI(curresult,16,4) << SCI(cursigma,11,2);
        for (const auto& b : _best)
            *_oiter << SCI(b.value,21,4) << SCI(b.error,11,2) << SCI(b.chisq,14,2);
        *_oiter << std::endl;
    }
    return *this;
}

// Print Vegas's grid to its output stream
const Vegas& Vegas::printGrid() const
{
    if (_ogrid && _ogrid->good()) {
        *_ogrid << _dim << " " << _bins << std::endl;
        for (size_t j = 0; j < _dim; ++j)
        {
            *_ogrid << SCI(j,2,0) << " ";
            for (size_t i = 0; i <= _bins; ++i)
                *_ogrid << SCI(_grid[i][j],11,4) << " ";
            *_ogrid << "\n";
        }
        *_ogrid << std::endl;
    }
    return *this;
}

// Print the piecewise probability distribution
const Vegas& Vegas::printDist() const
{
    if (_odist && _odist->good()) {
        for (size_t j = 0; j < _dim; ++j)
        {
            *_odist << "\n axis " << j << "\n\n";
            for (size_t i = 0; i < _bins; ++i)
            {
                *_odist << "weight[" << SCI(_grid[i][j],9,2);
                *_odist << "," << SCI(_grid[i+1][j],9,2) << "] = ";
                *_odist << SCI(_distr[i][j],11,2) << "\n";
            }
            *_odist << "\n";
        }
        *_odist << std::endl;
    }
    return *this;
}

// Status clearing
//----------------------------------------------------------------------------------------

// Discard all partial results computed so far
Vegas& Vegas::clearPartials()
{
    _iter = 0;
    _evals = 0;
    _best.clear();
    _sumVals.clear();
    _sumWgts.clear();
    _sumSqrs.clear();
    return *this;
}

// Resets grid to be uniformly spaced, preserving # of bins and dimensions
Vegas& Vegas::resetGrid()
{
    for (size_t b = 0; b < _grid.size(); ++b)
        for (size_t d = 0; d < _grid[b].size(); ++d)
            _grid[b][d] = static_cast<double>(b)/(_grid.size()-1.);
    return *this;
}

// Clears the probability distribution
Vegas& Vegas::clearDistr()
{
    for (size_t b = 0; b < _distr.size(); ++b)
        for (size_t d = 0; d < _distr[b].size(); ++d)
            _distr[b][d] = 0.;
    return *this;
}

// Parameter setting
//----------------------------------------------------------------------------------------

// Set dimension of integration
Vegas& Vegas::setDimension(const size_t& dim, const size_t& newMaxBins)
{
    if (dim != 0) _dim = dim;
    else std::cerr << "Cannot set dimension of integration to zero!" << std::endl;
    _callsXiter = 0;
    setMaxBins(newMaxBins);
    _x.resize(_dim,0.);
    _bin.resize(_dim,0);
    return *this;
}

// Set the damping exponent for grid adaptation
Vegas& Vegas::setAlpha(const double& alpha)
{
    _alpha = alpha;
    return *this;
}

// Set the output stream for general information
Vegas& Vegas::setInfoStream(std::ostream* infostream)
{
    _oinfo = infostream;
    return *this;
}

// Set the output stream for iteration-by-iteration results
Vegas& Vegas::setIterStream(std::ostream* iterstream)
{
    _oiter = iterstream;
    return *this;
}

// Set the output stream for the grid
Vegas& Vegas::setGridStream(std::ostream* gridstream)
{
    _ogrid = gridstream;
    return *this;
}

// Set the output stream for the grid's probability distribution
Vegas& Vegas::setDistStream(std::ostream* diststream)
{
    _odist = diststream;
    return *this;
}

// Set maximum number of bins
Vegas& Vegas::setMaxBins(const size_t& maxBins)
{
    if (maxBins != 0) _maxBins = maxBins;
    else std::cerr << "Cannot set maximum # of bins to zero!" << std::endl;
    _grid.resize(boost::extents[_maxBins+1][_dim]);
    _bins = _maxBins;
    resetGrid();
    _distr.resize(boost::extents[_maxBins][_dim]);
    clearDistr();
    clearPartials();
    if (_callsXiter != 0) setCalls(_callsXiter);
    return *this;
}

// Set maximum number of iterations.
Vegas& Vegas::setMaxIter(const size_t& maxIter)
{
    _maxIter = maxIter;
    return *this;
}

// Set target errors
Vegas& Vegas::setTarget(const double& abs, const double& rel)
{
    _absError = abs;
    _relError = rel;
    return *this;
}

// Freeze the integration grid
Vegas& Vegas::freeze()
{
    _frozen = true;
    return *this;
}

// Unfreeze the integration grid
Vegas& Vegas::unfreeze()
{
    _frozen = false;
    return *this;
}

// According to the number of calls,
// determine whether to use importance or stratified sampling.
Vegas& Vegas::setCalls(const size_t& calls)
{
    size_t bins = _maxBins;
    _boxes = 1;
    
    _useStrat = false;
    if (_tryStrat) {
        // Number of boxes per axis needed to have at least 2 points each
        size_t boxes = floor(pow(0.5*calls,1./_dim));
        // If boxes group together at least 2 bins per axis
        if (2*boxes >= _maxBins) {
            _useStrat = true;
            
            size_t boxXbin = std::max<size_t>(boxes/_maxBins,1);
            bins = std::min(boxes/boxXbin, _maxBins);
            _boxes = boxXbin * bins;
        }
    }
    
    const double totBoxes = ipow(static_cast<double>(_boxes),_dim);
    _callsXbox = std::max<size_t>(calls/totBoxes,2);
    _callsXiter = _callsXbox*totBoxes;
    _totBins = ipow(static_cast<double>(bins),_dim);
    
    // Adjust number of bins preserving bin density
    if (bins != _bins) _resizeGrid(bins);

    return *this;
}

// Read the integration grid from a given input stream
Vegas& Vegas::readGrid(std::istream& input)
{
    if (!input.good()) {
        std::cerr << "Could not read grid, input stream not good()" << std::endl;
        return *this;
    }
    size_t dim, bins, foo;
    input >> dim;
    input >> bins;
    setDimension(dim,bins);
    for (size_t d = 0; d < dim; ++d)
    {
        // discard the number of the dimension
        input >> foo;
        for (size_t b = 0; b < bins+1; ++b)
            input >> _grid[b][d];
    }
    return *this;
}


// Integration
//----------------------------------------------------------------------------------------

// Perform a single iteration
const Vegas::Estimates& Vegas::doIteration(const VFunction& f)
{
    Point sumVal; // Estimate of the integral from this iteration alone
    Point sumSqr; // Temporary Sum of Squares
    size_t comp = 0; // Number of components of the function
    std::vector<Grid> distrs; // Factored probabilities for function components
    Point boxmean; // Current box mean
    Point boxvar;  // Current box variance
    
    clearDistr();
    
    for (size_t boxn = 0; boxn < ipow(_boxes,_dim); ++boxn)
    {
        
        for (double& bmi : boxmean) bmi = 0.;
        for (double& bvi : boxvar ) bvi = 0.;
        
        for (size_t k = 0; k < _callsXbox; ++k)
        {
            // Evaluate integrand
            const double cubeVol = _shootPoint(boxn);
            const double weight = _totBins*cubeVol;
            Point y = f(_x,weight);
            if (comp == 0) {
                comp = y.size();
                boxmean.resize(comp,0.);
                boxvar.resize(comp,0.);
                distrs.resize(comp,_distr);
                sumVal.resize(comp,0.);
                sumSqr.resize(comp,0.);
            }
            const double wgt_calls = weight/_callsXiter;
            for (double& yi : y)
                yi *= wgt_calls;
            ++_evals;
            // Compute mean and variance recursively
            for (size_t i = 0; i < comp; ++i)
            {
                const double dy = y[i]-boxmean[i];
                boxmean[i] += dy/(k+1.);
                boxvar[i]  += dy*dy*k/(k+1.);
            }
            // Importance sampling: distribution proportional to square
            if (!_useStrat)
                for (size_t i = 0; i < comp; ++i)
                    _addDistr(distrs[i],_bin,y[i]*y[i]);
        }
        
        for (size_t i = 0; i < comp; ++i)
        {
            sumVal[i] += boxmean[i]*_callsXbox;
            const double boxsumsqr = boxvar[i]*_callsXbox;
            sumSqr[i] += boxsumsqr;
            // Stratification sampling: distribution proportional to variance
            if (_useStrat) _addDistr(distrs[i],_bin,boxsumsqr);
        }
        
    }
    
    if (_best.size() == 0) {
        _best.resize(comp);
        _sumWgts.resize(comp,0.);
        _sumVals.resize(comp,0.);
        _sumSqrs.resize(comp,0.);
    }
    Point var(comp,0.);
    // Compute results for this iteration
    for (size_t i = 0; i < comp; ++i)
    {
        Estimate& best = _best[i];
        var[i] = sumSqr[i]/(_callsXbox-1.);
        double wgt(0.);
        if (var[i] > 0) wgt = 1./var[i];
        else if (_sumWgts[i] > 0) wgt = _sumWgts[i]/best.ndof;
        // If the estimate of this integral is not zero
        if (wgt > 0) {
            ++best.ndof;
            // Compute the chi square using stable recurrence
            if (best.ndof == 1) {
                best.chisq = 0;
            } else {
                const double m = (_sumWgts[i] > 0) ? (_sumVals[i]/_sumWgts[i]) : 0;
                const double q = sumVal[i] - m;
                best.chisq *= best.ndof-2.;
                best.chisq += (wgt / (1+(wgt/_sumWgts[i]))) * q*q;
                best.chisq /= best.ndof-1.;
            }
            // Update partial sums
            _sumWgts[i] += wgt;
            _sumVals[i] += sumVal[i] * wgt;
            _sumSqrs[i] += sumVal[i] * sumVal[i] * wgt;
            // Update best estimate
            best.value = _sumVals[i] / _sumWgts[i];
            best.error = sqrt(1./_sumWgts[i]);
        } else {
            best.value += (sumVal[i]-best.value)/(_iter+1.);
            best.error = 0.;
        }
        // Contribute to the sampling distribution
        const double norm = fabs(best.value); // Thomas Hahn's choice
        if (norm != 0)
            for (size_t i1 = 0; i1 < _distr.size(); ++i1)
                for (size_t i2 = 0; i2 < _distr[i1].size(); ++i2)
                    _distr[i1][i2] += distrs[i][i1][i2]/norm;
    }
    
    ++_iter;
    if (!_frozen) _refineGrid();
    
    std::transform(
                   var.cbegin(),
                   var.cend(),
                   var.begin(),
                   static_cast<double(*)(double)>(std::sqrt)
                   );
    printIter(sumVal,var);
    printGrid();
    printDist();
    
    return _best;
    
}

// Perform a single iteration
const Vegas::Estimate& Vegas::doIteration(const Function& f)
{
    double sumVal = 0.; // Estimate of the integral from this iteration alone
    double sumSqr = 0.; // Temporary Sum of Squares

    clearDistr();
    
    for (size_t boxn = 0; boxn < ipow(_boxes,_dim); ++boxn)
    {
        
        double boxmean = 0.;
        double boxvar = 0.;
        
        for (size_t k = 0; k < _callsXbox; ++k)
        {
            // Evaluate integrand
            const double cubeVol = _shootPoint(boxn);
            const double weight = _totBins*cubeVol;
            const double wgt_calls = weight/_callsXiter;
            double y = wgt_calls*f(_x,weight);
            ++_evals;
            // Compute mean and variance recursively
            {
                const double dy = y-boxmean;
                boxmean += dy/(k+1.);
                boxvar  += dy*dy*k/(k+1.);
            }
            // Importance sampling: distribution proportional to square
            if (!_useStrat) _addDistr(_distr,_bin,y*y);
        }
        
        sumVal += boxmean*_callsXbox;
        const double boxsumsqr = boxvar*_callsXbox;
        sumSqr += boxsumsqr;
        // Stratification sampling: distribution proportional to variance
        if (_useStrat) _addDistr(_distr,_bin,boxsumsqr);
        
    }
    
    if (_best.size() == 0) {
        _best.resize(1);
        _sumWgts.resize(1,0.);
        _sumVals.resize(1,0.);
        _sumSqrs.resize(1,0.);
    }
    // Compute results for this iteration
    Estimate& best = _best[0];
    double var = sumSqr/(_callsXbox-1.);
    double wgt(0.);
    if (var > 0) wgt = 1./var;
    else if (_sumWgts[0] > 0) wgt = _sumWgts[0]/best.ndof;
    // If the estimate of this integral is not zero
    if (wgt > 0) {
        ++best.ndof;
        // Compute the chi square using stable recurrence
        if (best.ndof == 1) {
            best.chisq = 0;
        } else {
            const double m = (_sumWgts[0] > 0) ? (_sumVals[0]/_sumWgts[0]) : 0;
            const double q = sumVal - m;
            best.chisq *= best.ndof-2.;
            best.chisq += (wgt / (1+(wgt/_sumWgts[0]))) * q*q;
            best.chisq /= best.ndof-1.;
        }
        // Update partial sums
        _sumWgts[0] += wgt;
        _sumVals[0] += sumVal * wgt;
        _sumSqrs[0] += sumVal * sumVal * wgt;
        // Update best estimate
        best.value = _sumVals[0] / _sumWgts[0];
        best.error = sqrt(1./_sumWgts[0]);
    } else {
        best.value += (sumVal-best.value)/(_iter+1.);
        best.error = 0.;
    }
    
    ++_iter;
    if (!_frozen) _refineGrid();
    
    printIter(sumVal,std::sqrt(var));
    printGrid();
    printDist();
    
    return _best[0];
    
}

// Perform integration
const Vegas::Estimates& Vegas::integrate(const VFunction& f)
{
    
    if (_callsXiter == 0) {
        std::cerr << "Please set the number of calls per iteration." << std::endl;
        return _best;
    }
    printInfo();
    printGrid();
    double worstAbs;
    double worstRel;
    while (
           !_iter || ( // first iteration or
                      _iter < _maxIter && (
                                           worstAbs > _absError &&
                                           worstRel > _relError
                                           )
                      )
           )
    {
        doIteration(f);
        worstAbs = _best[0].error;
        worstRel = _best[0].error/fabs(_best[0].value);
        for (size_t i = 1; i < _best.size(); ++i)
        {
            const double& absi = _best[i].error;
            const double reli = absi/fabs(_best[i].value);
            if (absi > worstAbs) worstAbs = absi;
            if (reli > worstRel) worstRel = reli;
        }
    }
    
    return _best;
}

// Perform integration
const Vegas::Estimate& Vegas::integrate(const Function& f)
{
    
    if (_callsXiter == 0) {
        std::cerr << "Please set the number of calls per iteration." << std::endl;
        if (_best.empty()) _best.resize(1);
        return _best[0];
    }
    printInfo();
    printGrid();
    while (
           !_iter || ( // first iteration or
                      _iter < _maxIter && (
                                           _best[0].error > _absError &&
                                           _best[0].error > _relError*fabs(_best[0].value)
                                           )
                      )
           ) doIteration(f);
    return _best[0];
}

// Auxiliary functions
//----------------------------------------------------------------------------------------

// Smoothens the probability distribution in the d-th dimension
// averaging each bin with its nearest neighbors
Vegas& Vegas::_smoothenDistr(const size_t& d)
{
    double oldbin = _distr[0][d];
    double newbin = _distr[1][d];
    // Average the first two bins
    _distr[0][d] = (oldbin + newbin) / 2;
    // Average bins in the middle
    for (size_t i = 1; i < _bins - 1; ++i)
    {
        const double rc = oldbin + newbin;
        oldbin = newbin;
        newbin = _distr[i+1][d];
        _distr[i][d] = (rc + newbin) / 3;
    }
    // Average the last two bins
    _distr[_bins-1][d] = (oldbin + newbin) / 2;
    // Return the total
    return *this;
}

// Add a value in a hypercube to all matching bins in distr
Vegas::Grid& Vegas::_addDistr(Grid& distr, const Coords& bin, const double& y)
{
    for (size_t j = 0; j < _dim; ++j)
        distr[bin[j]][j] += y;
    return distr;
}

// Shoot a random point in a given box,
// store its coordinates in _x and its hypercube in _bin
double Vegas::_shootPoint(size_t boxn)
{
    
    double vol = 1.;
    
    _x.resize(_dim); // size might have changed during integrand call
    for (size_t j = 0; j < _dim; ++j)
    {
        // skip random numbers that are exactly 0. or 1.
        double ran = 0.;
        while (ran == 0. || ran == 1.) ran = _random();
        // (boxn%_boxes) gives the j-th coordinate of this box,
        // stepping through boxes like {0,0}, {1,0}, {2,0}, {0,1}, {1,1}, {2,1}, ...
        const double z = (((boxn%_boxes)+ran) / _boxes) * _bins;
        // preparing for the next dimension
        boxn /= _boxes;
        // the bin coordinate in the j-th dimension is the integer part of z
        _bin[j] = z;
        
        const double bin_width = _grid[_bin[j]+1][j] - _grid[_bin[j]][j];
        _x[j] = _grid[_bin[j]][j] + (z-_bin[j]) * bin_width;
        
        vol *= bin_width;
    }
    
    return vol;
}

// Redistribute bins along an axis according to some weights
Vegas& Vegas::_redistribute(
                            const std::vector<double>& wgts,
                            const size_t& d,
                            const double& xtr
                            )
{
    
    const size_t bins = wgts.size();
    const double weightXbin = std::accumulate(wgts.cbegin(),wgts.cend(),xtr) / bins;
    std::vector<double> foo(bins+1);
    
    double xold(0.), xnew(0.), dw(0.);
    size_t i = 1;
    
    for (size_t k = 0; k < _bins; ++k)
    {
        const double deltaw = k < bins ? wgts[k] : 1.;
        dw += deltaw;
        xold = xnew;
        xnew = _grid[k+1][d];
        
        for (; dw > weightXbin; ++i)
        {
            dw -= weightXbin;
            foo[i] = xnew - (xnew - xold) * dw / deltaw;
        }
    }
    
    for (size_t k = 1; k < bins; ++k)
        _grid[k][d] = foo[k];
    // In case the number of bins has changed
    _grid[bins][d] = 1.;
    return *this;
    
}

// Resize grid for a different number of bins
Vegas& Vegas::_resizeGrid(const size_t& bins)
{
    const std::vector<double> weights(bins,1.);
    for (size_t j = 0; j < _dim; ++j)
        _redistribute(weights,j,_bins-bins);
    _bins = bins;
    _grid.resize(boost::extents[bins+1][_dim]);
    return *this;
}

// Refine the grid according to the probability distribution in _distr
Vegas& Vegas::_refineGrid()
{
    
    std::vector<double> _weight(_bins);
    // In each direction separately
    for (size_t j = 0; j < _dim; ++j)
    {
        if (_smoothen) _smoothenDistr(j);
        double totdistr = 0.;
        for (size_t k = 0; k < _bins; ++k) totdistr += _distr[k][j];
        // If all weights are zero, do nothing
        // (rather than collapsing all bins to zero)
        if (totdistr == 0.) return *this;
        // Assign weights to bins
        for (size_t i = 0; i < _bins; ++i)
        {
            _weight[i] = 0.;
            
            if (_distr[i][j] > 0) {
                const double oldg = totdistr / _distr[i][j];
                // Weight according to Lepage's formula
                _weight[i] = pow(((oldg-1.) / (oldg*log(oldg))), _alpha);
            }
#ifdef DEBUG
            _out << "weight[" << i << "] = " << _weight[i] << std::endl;
#endif
        }
        // Resize bins according to weights
        _redistribute(_weight,j);
    }
    return *this;
}

// Non-member functions
//----------------------------------------------------------------------------------------

// Overloading of << for integral estimates
std::ostream& operator<<(std::ostream& str, const Vegas::Estimate& res)
{
    return str << res.value << " +- " << res.error
               << ", chisq/it(dof) " << res.chisq << "(" << res.ndof << ")";
}

// Overloading of << for vectors of integral estimates
std::ostream& operator<<(std::ostream& str, const Vegas::Estimates& res)
{
    for (size_t i = 0; i < res.size(); ++i)
    {
        if (res.size() > 1) str << "Component " << i << ": ";
        str << res[i] << std::endl;
    }
    return str;
}
