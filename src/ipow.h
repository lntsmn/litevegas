/**
 *
 * @file    ipow.h
 * @author  Simone Lionetti
 * @date    May 2016
 *
 */

#ifndef LITEVEGAS_IPOW_H
#define LITEVEGAS_IPOW_H

#include <cstdlib>

/**
 * @fn    ipow
 * @brief Overload of pow for integer exponents and any argument
 */

template <class BaseT>
BaseT ipow(const BaseT& base, const int n)
{
    BaseT result(1), bas(base);
    size_t exp = abs(n);
    while (exp)
    {
        if (exp&1) result *= bas;
        exp >>= 1;
        bas *= bas;
    }
    if (n>=0) return result;
    else return BaseT(1)/result;
}

#endif
