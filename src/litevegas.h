/**
 *
 * @file    litevegas.h
 * @author  Simone Lionetti
 * @date    May 2016
 *
 */

#ifndef LITEVEGAS_H
#define LITEVEGAS_H

#include <boost/multi_array.hpp>

#include <algorithm>    // std::min, std::max
#include <iostream>     // std::cout
#include <iomanip>      // std::setw
#include <math.h>       // pow
#include <vector>       // std::vector

class Vegas
{
    
public:
    
    /// @name Type declarations
    /// @{
    
    /// @struct Estimate
    /// @brief  Aggregate type for estimate of integral
    struct Estimate
    {
        double value;
        double error;
        double chisq;
        size_t ndof;
    };
    
    using Point = std::vector<double>;
    using Coords = std::vector<size_t>;
    using Estimates = std::vector<Estimate>;
    using Function = std::function<double(Point& x,const double& w)>;
    using VFunction = std::function<Point(Point& x,const double& w)>;
    using Random = std::function<double(void)>;
    using Grid = boost::multi_array<double,2>;
    
    /// @}
    
private:
    
    /// @name Private data members
    /// @{
    
    size_t _dim;        ///< Number of dimensions of integration

    double _alpha;      ///< Damping exponent for grid changes
    Random& _random;    ///< Random number generator

    bool _tryStrat;     ///< Try to use stratified sampling if # of calls is high enough
    bool _useStrat;     ///< Stratification adaption on
    bool _frozen;       ///< Freeze the adaptation grid
    bool _smoothen;     ///< Smoothen out the sampling probability distribution
    
    size_t _maxBins;    ///< Maximum number of bins per axis
    size_t _bins;       ///< Actual # of bins per axis, has to agree with _grid.size()
    size_t _totBins;    ///< Actual # of hypercubes, equal to _bins^(_dim)
    size_t _boxes;      ///< Number of boxes (bin groupings)
    Grid _grid;         ///< Grid on the integration range
    Grid _distr;        ///< Step-wise, factored probability for sampling
    
    size_t _callsXiter; ///< Number of calls per iteration
    size_t _callsXbox;  ///< Number of calls per iteration per box
    
    size_t _maxIter;    ///< Maximum number of iterations
    double _absError;   ///< Target absolute error
    double _relError;   ///< Target relative error
    
    size_t _iter;       ///< Number of completed iterations
    size_t _evals;      ///< Number of integrand evaluations so far
    Estimates _best;    ///< Storage of the best estimate based on previous iterations
    Point _sumVals;     ///< Storage of weighted sum of estimates from previous iterations
    Point _sumWgts;     ///< Storage for the sum of weights from previous iterations
    Point _sumSqrs;     ///< Storage of weighted sum of squares from previous iterations
    
    std::ostream* _oinfo; ///< General information output stream
    std::ostream* _oiter; ///< Iteration output stream
    std::ostream* _ogrid; ///< Grid output stream
    std::ostream* _odist; ///< Probability distribution output stream
    
    Point _x;       ///< Storage space for random points
    Coords _bin;    ///< Storage space for bin coordinates
    
    /// @}

public:
    
    /// @name Constructors and destructor
    /// @{
    
    /// Constructor with data
    Vegas(
          Random& random,
          const size_t& dim = 1,
          const size_t& maxBins = 50,
          const bool tryStratification = true
          );
    
    /// Destructor
    virtual ~Vegas() {}
    
    /// @}
    
    /// @name Printing
    /// @{
    
    /// @brief Print setup parameters and the head for iteration-by-iteration results
    const Vegas& printInfo() const;
    
    /// @brief Print line with current iteration's result
    /// @param curresult The current iteration estimate for the integral
    /// @param cursigma  The current iteration estimate for the standard deviation
    const Vegas& printIter(const Point& curresult, const Point& cursigma) const;
    
    /// @brief Print line with current iteration's result
    /// @param curresult The current iteration estimate for the integral
    /// @param cursigma  The current iteration estimate for the standard deviation
    const Vegas& printIter(const double& curresult, const double& cursigma) const;
    
    /// @brief Print Vegas's grid to its output stream
    const Vegas& printGrid() const;
    
    /// @brief Print the piecewise probability distribution
    const Vegas& printDist() const;
    
    /// @}

    /// @name Queries
    /// @{
    
    /// @brief Number of completed integrand evaluations
    inline const size_t& evaluations() const {return _evals;}
    
    /// @brief Number of completed iterations
    inline const size_t& iterations() const {return _iter;}
    
    /// @brief Best estimate for the integral
    inline const Estimates& best() const {return _best;}
    
    /// @}

    /// @name Status clearing
    /// @{
    
    /// @brief Discard all previous iteration (but keep the grid)
    Vegas& clearPartials();
    
    /// @brief Resets grid to be uniformly spaced, preserving # of bins and dimensions
    Vegas& resetGrid();
    
    /// @brief Clears the probability distribution
    Vegas& clearDistr();
    
    /// @}
    
    /// @name Parameter setting
    /// @{
    
    /// @brief Set dimension of integration
    ///        This will discard the grid and all previous information.
    /// @param dim The new dimension of integration
    Vegas& setDimension(const size_t& dim, const size_t& newMaxBins);
    
    /// @brief Set dimension of integration
    ///        This will discard the grid and all previous information.
    /// @param dim The new dimension of integration
    inline Vegas& setDimension(const size_t& dim) {return setDimension(dim,_maxBins);}

    /// @brief Set the damping exponent for grid adaptation
    Vegas& setAlpha(const double& alpha);
    
    /// @brief Set the output stream for general information
    Vegas& setInfoStream(std::ostream* infostream);
    
    /// @brief Set the output stream for iteration-by-iteration results
    Vegas& setIterStream(std::ostream* iterstream);
    
    /// @brief Set the output stream for the grid
    Vegas& setGridStream(std::ostream* gridstream);
    
    /// @brief Set the output stream for the grid's probability distribution
    Vegas& setDistStream(std::ostream* diststream);
    
    /// @brief Set maximum number of bins
    ///        This will discard the grid and all previous information.
    /// @param maxBins New maximum number of subdivisions per axis
    Vegas& setMaxBins(const size_t& maxBins);
    
    /// @brief Set maximum number of iterations
    /// @param maxIter New maximum number of iterations
    Vegas& setMaxIter(const size_t& maxIter);
    
    /// @brief Set target errors
    /// @param abs Target absolute error
    /// @param rel Target relative error
    Vegas& setTarget(const double& abs, const double& rel);
    
    /// @brief Freeze the integration grid
    Vegas& freeze();
    
    /// @brief Unfreeze the integration grid
    Vegas& unfreeze();
    
    /// @brief According to the number of calls,
    ///        determine whether to use importance or stratified sampling.
    /// @param calls Suggested number of integrand calls per iteration
    /// @note  Actual number of calls used may differ slightly for better distribution
    Vegas& setCalls(const size_t& calls);
    
    /// @brief Read the integration grid from a given input stream
    Vegas& readGrid(std::istream& input);
    
    /// @}
    
    /// @name Integration
    /// @{
    
    /// @brief Perform a single iteration for a vector integrand
    /// @param f Integrand function
    const Estimates& doIteration(const VFunction& f);

    /// @brief Perform a single iteration for a scalar integrand
    /// @param f Integrand function
    const Estimate& doIteration(const Function& f);
    
    /// @brief Perform integration for a vector integrand
    /// @param f Integrand function
    ///        This function does at least one iteration if none has been done before,
    ///        and stops when one of the target criteria is met.
    const Estimates& integrate(const VFunction& f);
    
    /// @brief Perform integration for a scalar integrand
    /// @param f Integrand function
    ///        This function does at least one iteration if none has been done before,
    ///        and stops when one of the target criteria is met.
    const Estimate& integrate(const Function& f);
    
    /// @}
    
private:
    
    /// @name Auxiliary functions
    /// @{
    
    /// @brief Smoothens the probability distribution in the d-th dimension
    ///        averaging each bin with its nearest neighbors
    /// @param d The dimension along which smoothening is performed
    /// @note  Changes the total weight because of boundaries. I don't like this.
    Vegas& _smoothenDistr(const size_t& d);
    
    /// @brief Add a value in a hypercube to all matching bins in factorized grid
    /// @param distr Factorized probability density to be modified
    /// @param bin   Coordinates of the hypercube the value should be added in
    /// @param y     Value to be added
    Grid& _addDistr(Grid& distr, const Coords& bin, const double& y);
    
    /// @brief  Shoot a random point in a given box,
    ///         store its coordinates in _x and its hypercube in _bin
    /// @return Volume of the hypercube to be used as a weight
    /// @param  boxn The index of the box where the point must fall, from 0 to _boxes^_dim
    double _shootPoint(size_t boxn);
    
    /// @brief Redistribute bins along an axis according to some weights
    /// @param wgts The vector of weights associated with each bin
    /// @param d    The dimension along which redistribution is performed
    /// @param xtr  Extra weight for grid resizing
    Vegas& _redistribute(
                         const std::vector<double>& wgts,
                         const size_t& d,
                         const double& xtr = 0.
                         );
    
    /// @brief Resize grid for a different number of bins
    Vegas& _resizeGrid(const size_t& bins);

    /// @brief Refine the grid according to the probability distribution in _distr
    Vegas& _refineGrid();
    
    /// @}
    
};

/// @brief Overloading of << for integral estimates
/// @param str Output stream to print the result to
/// @param res The integral estimate to be printed
std::ostream& operator<<(std::ostream& str, const Vegas::Estimate& res);

/// @brief Overloading of << for vectors of integral estimates
/// @param str Output stream to print the result to
/// @param res The integral estimates to be printed
std::ostream& operator<<(std::ostream& str, const Vegas::Estimates& res);

#endif
